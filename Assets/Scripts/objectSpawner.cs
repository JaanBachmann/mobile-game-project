﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class objectSpawner : NetworkBehaviour {
    public GameObject spawnPrefab;
    private int objectNumber;
    private GameObject spawnPositions;
    private Transform spawnTransform;

    public void SpawnSpawns() {
        spawnPositions = GameObject.Find("SpawnPositions");
        objectNumber = spawnPositions.transform.childCount;
		for (int i=0;i<objectNumber;i++) {
            spawnTransform = spawnPositions.transform.GetChild(i);
            var pos = new Vector3(spawnTransform.position.x, 0.5f,spawnTransform.position.z);
            var rotation = Quaternion.Euler(0f, 0f, 0f);
            var obj = (GameObject)Instantiate(spawnPrefab, pos, rotation);
            NetworkServer.Spawn(obj);
        }
	}
}
