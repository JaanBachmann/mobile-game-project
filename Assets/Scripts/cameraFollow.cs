﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraFollow : MonoBehaviour {
    public Transform playerTransform;
    public int depth;
    public int width;
 
    void Update() {
        if (playerTransform != null) {
            if (playerTransform.position != new Vector3(0f,0f,0f)) {
                transform.position = playerTransform.position + new Vector3(0,width,depth);
            }
        }
    }
    public void setTarget(Transform target) {
        playerTransform = target;
    }
}
