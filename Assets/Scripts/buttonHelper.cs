﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class buttonHelper : MonoBehaviour {
    public GameObject dataManager;
    public string buttonType;
    public Button Next;
    public Button Prev;

    private saveManager dataScript;
    private int scriptState;

	void Awake () {
        dataScript = dataManager.GetComponent<saveManager>();
	}
	
	void Update () {
        if (buttonType == "hat") { scriptState = dataScript.state.hat; }
        if (buttonType == "face") { scriptState = dataScript.state.face; }
        if (buttonType == "top") { scriptState = dataScript.state.top; }
        if (buttonType == "bot") { scriptState = dataScript.state.bot; }

        if (scriptState == 1) {
            Prev.gameObject.SetActive(false);
            Next.gameObject.SetActive(true);
        }
        else if (scriptState == 3) {
            Prev.gameObject.SetActive(true);
            Next.gameObject.SetActive(false);
        }
        else {
            Prev.gameObject.SetActive(true);
            Next.gameObject.SetActive(true);
        }
	}
}
