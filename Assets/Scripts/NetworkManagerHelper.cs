﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;

public class NetworkManagerHelper : NetworkManager
{
    objectSpawner spawner;
    public Slider playerSlider;
    public Button buttonStart;
    public Button buttonFind;
    public Button buttonStop;
    public Button buttonBack;
    public Image background;
    public GameObject objSpawner;
    public float sliderValue;
    private const string ROOM_NAME = "default";

    private void Awake() {
        spawner = objSpawner.GetComponent<objectSpawner>();
    } 
    private void Initialize() {
        SetMatchHost("mm.unet.unity3d.com", 443, false);
        StartMatchMaker();
    }

    public void StartMatchMaking() {
        GameStates("start");
        Initialize();
        CreateInternetMatch(ROOM_NAME);
    }
    public void JoinMatch() {
        GameStates("join");
        Initialize();
        FindInternetMatch(ROOM_NAME);
    }

    public void StopMatch() {
        Debug.Log("Attempting to stop...");
        GameStates("stop");
        StopHost();
    }

    public void CreateInternetMatch(string matchName) {
        Debug.Log("CreateInternetMatch=" + matchName);
        matchMaker.CreateMatch(matchName, 4, true, "", "", "", 0, 0, OnInternetMatchCreate);
        Debug.Log("After!");
    }

    private void OnInternetMatchCreate(bool success, string extendedInfo, MatchInfo matchInfo) {
        Debug.Log("success=" + success);
        Debug.Log("extendedInfo=" + extendedInfo);
        Debug.Log("matchInfo=" + matchInfo);
        if (success) {
            MatchInfo hostInfo = matchInfo;
            NetworkServer.Listen(hostInfo, 9000);
            spawner.SpawnSpawns();
            StartHost(hostInfo);
        }
        else {
            Debug.LogError("Create match failed");
        }
    }

    public void FindInternetMatch(string matchName) {
        Debug.Log("matchmaker=" + matchMaker);
        matchMaker.ListMatches(0, 10, matchName, true, 0, 0, OnInternetMatchList);
    }

    //this method is called when a list of matches is returned
    private void OnInternetMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matches) {
        if (success) {
            if (matches.Count != 0) {
                matchMaker.JoinMatch(matches[matches.Count - 1].networkId, "", "", "", 0, 0, OnJoinInternetMatch);
            }
            else {
                Debug.Log("No matches in requested room!");
            }
        }
        else {
            Debug.LogError("Couldn't connect to matchmaker");
        }
    }

    //this method is called when your request to join a match is returned
    private void OnJoinInternetMatch(bool success, string extendedInfo, MatchInfo matchInfo) {
        if (success) {
            MatchInfo hostInfo = matchInfo;
            StartClient(hostInfo);
        }
        else {
            Debug.LogError("Join match failed");
        }
    }

    private void GameStates(string action) {
        if ( action == "start" ) {
            sliderValue = playerSlider.value;

            buttonStart.gameObject.SetActive(false);
            buttonFind.gameObject.SetActive(false);
            buttonBack.gameObject.SetActive(false);
            playerSlider.gameObject.SetActive(false);
            background.gameObject.SetActive(false);
            buttonStop.gameObject.SetActive(true);
        }
        else if ( action == "stop" ) {
            sliderValue = 2;

            buttonStart.gameObject.SetActive(true);
            buttonFind.gameObject.SetActive(true);
            buttonBack.gameObject.SetActive(true);
            playerSlider.gameObject.SetActive(true);
            background.gameObject.SetActive(true);
            buttonStop.gameObject.SetActive(false);
        }
        else if ( action == "join" ) {
            buttonStart.gameObject.SetActive(false);
            buttonFind.gameObject.SetActive(false);
            buttonBack.gameObject.SetActive(false);
            playerSlider.gameObject.SetActive(false);
            background.gameObject.SetActive(false);
            buttonStop.gameObject.SetActive(true);
        }
    }






}