﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class saveManager : MonoBehaviour {
    public static saveManager Instance { set; get; }
    public playerStuff state;

    private void Awake () {
        DontDestroyOnLoad(gameObject);
        Instance = this;
        Load();
	}
    public void Save() {
        PlayerPrefs.SetString("save",helperScript.Serialize<playerStuff>(state));
        Debug.Log("Face: " + state.face);
    }
    public void Load() {
        if (PlayerPrefs.HasKey("save")) {
            state = helperScript.Deserialize<playerStuff>(PlayerPrefs.GetString("save"));
        }
        else {
            state = new playerStuff();
            Save();
        }
        Debug.Log("Player: " + state.name);
    }
	public void ResetSave() {
        PlayerPrefs.DeleteKey("save");
    }
    public void changeItem(customParameters stuff) {
        if (stuff.itemType == "name") { state.name = stuff.valueString; }
        else if (stuff.itemType == "hat") {
            if (stuff.add) state.hat += 1;
            else state.hat -= 1;
        }
        else if (stuff.itemType == "face") {
            if (stuff.add) state.face += 1;
            else state.face -= 1;
        }
        else if (stuff.itemType == "top") {
            if (stuff.add) state.top += 1;
            else state.top -= 1;
        }
        else if (stuff.itemType == "bot") {
            if (stuff.add) state.bot += 1;
            else state.bot -= 1;
        }
        Save();
    }
}
