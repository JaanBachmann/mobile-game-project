﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class customParameters : MonoBehaviour {
    public string itemType;
    public InputField textBox;
    public bool add;
    public string valueString;
    void Update() {
        if (textBox != null) {
            valueString = textBox.text;
        }
    }
}
