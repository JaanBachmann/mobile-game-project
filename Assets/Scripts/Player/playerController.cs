﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class playerController : NetworkBehaviour {
    public playerCustomize customizer;
    public Material hatMaterial;
    public Material faceMaterial;
    public Material topMaterial;
    public Material botMaterial;

    [SyncVar] private Color hatColor;
    [SyncVar] private Color faceColor;
    [SyncVar] private Color topColor;
    [SyncVar] private Color botColor;

    public GameObject body;
    public GameObject hat;
    public GameObject circle;

    public float moveSpeed;
    public float maxSpeed;
    public float rotationSpeed;

    Rigidbody rigidBody;
    private GameObject sync;
    private playerSync syncScript;
    private GameObject manager;
    private GameObject cameraRig;
    private GameObject[] playersInGame;

    [SyncVar] Vector3 realPosition = Vector3.zero;
    [SyncVar] Quaternion realRotation;

    [SyncVar] public float playersRequired;
    [SyncVar] public bool canStart = false;
    [SyncVar] public bool canTag;
    [SyncVar] public int playerID;
    [SyncVar] public int myState;

    private int targetID;
    private int playerIndex;
    private bool startCheck = true;
    private bool canMove;
    private bool tagCheck;
    private bool coll;

    private float positionX;
    private float transformX;
    private float positionZ;
    private float transformZ;
    private float updateInterval;

	void Awake () {
        manager = GameObject.Find("NetworkManager");
        cameraRig = GameObject.Find("CameraRig");
        sync = GameObject.Find("Ground");
        syncScript = sync.GetComponent<playerSync>();
		rigidBody = GetComponent<Rigidbody>();
        playersInGame = GameObject.FindGameObjectsWithTag("Players");
        playerID = playersInGame.Length + 5;
	}
    public override void OnStartLocalPlayer() {
        cameraRig.GetComponentInChildren<cameraFollow>().setTarget(gameObject.transform);
    }
    public override void OnStartServer() {
        if (manager.GetComponent<NetworkManagerHelper>().sliderValue != 0) {
            playersRequired = manager.GetComponent<NetworkManagerHelper>().sliderValue;
        } else {
            playersRequired = 2;
        }
    }

    void Update() {
        if (isServer) {
            if (startCheck) {
                playersInGame = GameObject.FindGameObjectsWithTag("Players");
                if (playersRequired == (playersInGame.Length)) {
                    startCheck = false;
                    if (isLocalPlayer) { 
                        playerIndex = Random.Range(0, playersInGame.Length);
                        CmdSetChaser(playersInGame[playerIndex].GetComponent<playerController>().playerID);
                    }
                    canStart = true;
                }
            }
        }
        coll = true;
        if (playerID == syncScript.chaserID) {
            myState = 1;
            circle.SetActive(true);
        }
        else {
            myState = 0;
            circle.SetActive(false);
        }
        if (isLocalPlayer) {
            if (customizer.playerHat == 1) { hatColor = Color.red; }
            else if (customizer.playerHat == 2) { hatColor = Color.green; }
            else if (customizer.playerHat == 3) { hatColor = Color.blue; }
            hatMaterial.color = hatColor;
            if (customizer.playerFace == 1) { faceColor = hexColor(255,225,171,255); }
            else if (customizer.playerFace == 2) { faceColor = hexColor(124,89,27,255); }
            else if (customizer.playerFace == 3) { faceColor = hexColor(246,183,49,255); }
            faceMaterial.color = faceColor;
            if (customizer.playerTop == 1) { topColor = Color.black; }
            else if (customizer.playerTop == 2) { topColor = Color.white; }
            else if (customizer.playerTop == 3) { topColor = Color.gray; }
            topMaterial.color = topColor;
            if (customizer.playerBot == 1) { botColor = hexColor(20,80,139,255); }
            else if (customizer.playerBot == 2) { botColor = hexColor(36,56,75,255); }
            else if (customizer.playerBot == 3) { botColor = hexColor(155,171,203,255); }
            botMaterial.color = botColor;
            if (!canMove && canStart) {
                canMove = true;
            }
		    findPosition();
            if (canMove) {
		        if(Input.GetMouseButton(0)) { 
			        moveToPosition();
		        }
                else {
                    if (rigidBody.velocity != Vector3.zero) {
                        rigidBody.velocity = Vector3.Lerp(rigidBody.velocity, Vector3.zero, 0.1f);
                    }
                    if (rigidBody.angularVelocity != Vector3.zero) {
                        rigidBody.angularVelocity = Vector3.Lerp(rigidBody.angularVelocity, Vector3.zero, 0.1f);
                    }
                }
            }
            updateInterval += Time.deltaTime;
            if (updateInterval > 0.11f) {
                updateInterval = 0;
                CmdSync(transform.position, transform.rotation);
            }
        }
        else {
            transform.position = Vector3.Lerp(transform.position, realPosition, 0.1f);
            transform.rotation = Quaternion.Lerp(transform.rotation, realRotation, 0.1f);
        }
	}

    void findPosition() {
		Plane playerPlane = new Plane(Vector3.up, transform.position);
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		float distance = 0.0f;
		if (playerPlane.Raycast(ray, out distance)) {
			Vector3 targetPoint = ray.GetPoint(distance);
            positionX = roundToDecimal(targetPoint.x);
            transformX = roundToDecimal(transform.position.x);
            positionZ = roundToDecimal(targetPoint.z);
            transformZ = roundToDecimal(transform.position.z);
            if (positionX != transformX && positionZ != transformZ) {
			    Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);
			    transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.fixedDeltaTime);
            } else { canMove = false; }
        }
	}
	void moveToPosition() {
		rigidBody.AddForce(transform.forward * moveSpeed * 10);
        if (rigidBody.velocity.magnitude > maxSpeed) {
            rigidBody.velocity *= 0.7f;
        }
	}
    float roundToDecimal(float value) {
        value = Mathf.Round(value * 10) / 10;
        return value;
    }

    void OnTriggerStay(Collider target) {
        if (target.gameObject.tag.Equals("spawnPosition") == true) {
            Destroy(target.gameObject);
        }
    }
    private void OnCollisionEnter(Collision target) {
        if (!coll) { return; }
        coll = false;
        if (target.gameObject.tag.Equals("Players") == true) {
            if (myState == 1 && canTag) {
                if (isLocalPlayer) {
                    targetID = target.gameObject.GetComponent<playerController>().playerID;
                    Debug.Log("Player:" + playerID + "Target:" + targetID);
                    CmdSetChaser(targetID);
                }
                tagCheck = true;
                if (tagCheck) {
                    canTag = false;
                    tagCheck = false;
                    Debug.Log("UnTouchable - " + playerID);
                    StartCoroutine(unTouchable());
                }
            }
		}
    }

    [Command]
    void CmdSync(Vector3 position, Quaternion rotation) {
        realPosition = position;
        realRotation = rotation;
    }
    
    [ClientRpc]
    void RpcSetChaser(int targetrpc, bool start) {
        if (syncScript.canChange) {
            syncScript.chaserID = targetrpc;
            syncScript.canChange = false;
        }
    }
    [Command]
    void CmdSetChaser(int targetcmd) {
        RpcSetChaser(targetcmd, false);
    }
    IEnumerator unTouchable() {
        yield return new WaitForSeconds(3.0f);
        Debug.Log("Touchable - " + playerID);
        canTag = true;
        tagCheck = true;
    }
    public static Vector4 hexColor(float r, float g, float b, float a){
         Vector4 color = new Vector4(r/255, g/255, b/255, a/255);
         return color;
    }
}
