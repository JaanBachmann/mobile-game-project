﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class customPlayer : MonoBehaviour {
    public playerCustomize customizer;
    public Material hatMaterial;
    public Material faceMaterial;
    public Material topMaterial;
    public Material botMaterial;

    private Color hatColor;
    private Color faceColor;
    private Color topColor;
    private Color botColor;

    void Awake() {
        if (customizer.playerHat == 1) { hatColor = Color.red; }
        else if (customizer.playerHat == 2) { hatColor = Color.green; }
        else if (customizer.playerHat == 3) { hatColor = Color.blue; }
        hatMaterial.color = hatColor;
        if (customizer.playerFace == 1) { faceColor = hexColor(255,225,171,255); }
        else if (customizer.playerFace == 2) { faceColor = hexColor(124,89,27,255); }
        else if (customizer.playerFace == 3) { faceColor = hexColor(246,183,49,255); }
        faceMaterial.color = faceColor;
        if (customizer.playerTop == 1) { topColor = Color.black; }
        else if (customizer.playerTop == 2) { topColor = Color.white; }
        else if (customizer.playerTop == 3) { topColor = Color.gray; }
        topMaterial.color = topColor;
        if (customizer.playerBot == 1) { botColor = hexColor(20,80,139,255); }
        else if (customizer.playerBot == 2) { botColor = hexColor(36,56,75,255); }
        else if (customizer.playerBot == 3) { botColor = hexColor(155,171,203,255); }
        botMaterial.color = botColor;
	}
	void Update() {
		if (customizer.playerHat == 1) { hatColor = Color.red; }
        else if (customizer.playerHat == 2) { hatColor = Color.green; }
        else if (customizer.playerHat == 3) { hatColor = Color.blue; }
        hatMaterial.color = hatColor;
        if (customizer.playerFace == 1) { faceColor = hexColor(255,225,171,255); }
        else if (customizer.playerFace == 2) { faceColor = hexColor(124,89,27,255); }
        else if (customizer.playerFace == 3) { faceColor = hexColor(246,183,49,255); }
        faceMaterial.color = faceColor;
        if (customizer.playerTop == 1) { topColor = Color.black; }
        else if (customizer.playerTop == 2) { topColor = Color.white; }
        else if (customizer.playerTop == 3) { topColor = Color.gray; }
        topMaterial.color = topColor;
        if (customizer.playerBot == 1) { botColor = hexColor(20,80,139,255); }
        else if (customizer.playerBot == 2) { botColor = hexColor(36,56,75,255); }
        else if (customizer.playerBot == 3) { botColor = hexColor(155,171,203,255); }
        botMaterial.color = botColor;
	}
    public static Vector4 hexColor(float r, float g, float b, float a){
         Vector4 color = new Vector4(r/255, g/255, b/255, a/255);
         return color;
    }
}
