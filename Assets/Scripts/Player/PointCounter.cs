﻿
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PointCounter : MonoBehaviour {
    GameObject TextObject;
    Text countText;
    private int count;
	
	void Start () {
        TextObject = GameObject.Find("Count Text");
        countText = TextObject.GetComponent<Text>();
        count = 0;
        SetCountText ();
    }

    void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag.Equals("Players")) {
            count = count + 1;
            SetCountText ();
        }
	}
    void SetCountText () {
        countText.text = "Count: " + count.ToString();
    }
}

