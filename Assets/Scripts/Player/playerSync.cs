﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class playerSync : NetworkBehaviour {
    [SyncVar] public int chaserID;
    [SyncVar] public bool canChange;

    private void Update() {
        if (!canChange) {
            StartCoroutine(change());
        }
    }
    IEnumerator change() {
        yield return new WaitForSeconds(3.0f);
        canChange = true;
    }
}
