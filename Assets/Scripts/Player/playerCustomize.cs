﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class playerCustomize : MonoBehaviour {
    private string playerName;
    public int playerHat;
    public int playerFace;
    public int playerTop;
    public int playerBot;

    public InputField textField;
    public GameObject dataManager;
    private saveManager dataScript;

	private void Awake() {
        if (dataManager == null) { dataManager = GameObject.Find("PlayerData"); }
        dataScript = dataManager.GetComponent<saveManager>();
        playerName = dataScript.state.name;
        playerHat = dataScript.state.hat;
        playerFace = dataScript.state.face;
        playerTop = dataScript.state.top;
        playerBot = dataScript.state.bot;

        if (textField != null) { textField.text = playerName; }
	}
	void Update() {
		playerName = dataScript.state.name;
        playerHat = dataScript.state.hat;
        playerFace = dataScript.state.face;
        playerTop = dataScript.state.top;
        playerBot = dataScript.state.bot;
	}
}
