﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour
{

    public AudioSource sfxSource;
    public AudioSource musicSource;
    public static SoundManager instance = null;

    public float lowPitchRange = .95f;
    public float highPitchRange = 1.05f;

    public AudioClip[] musicGameplay;    
    public AudioClip musicMenu;
    public AudioClip sfxMove;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    public void PlayMusic(AudioClip clip)
    {
        Debug.Log("PlayMusic(" + clip + ")");
    
        musicSource.Play();
    }

    public void PlayRandomMusic() {
        int randomIndex = Random.Range(0, musicGameplay.Length);
        musicSource.clip = musicGameplay[randomIndex];
        musicSource.Play();

        Debug.Log("PlayMusic(" + musicSource.clip + ")");
    }

    public void PlaySFX(AudioClip clip)
    {
        Debug.Log("PlaySFX(" + clip + ")");
        sfxSource.clip = clip;
        sfxSource.Play();
    }

    public void RandomizeSfx(params AudioClip[] clips)
    {
        int randomIndex = Random.Range(0, clips.Length);
        float randomPitch = Random.Range(lowPitchRange, highPitchRange);

        sfxSource.pitch = randomPitch;
        sfxSource.clip = clips[randomIndex];
        sfxSource.Play();
    }

}